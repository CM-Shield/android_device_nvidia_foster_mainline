TARGET_TEGRA_AUDIO    := tinyhal
TARGET_TEGRA_CAMERA   := none
TARGET_TEGRA_CEC      := none
TARGET_TEGRA_KERNEL   ?= 54
TARGET_TEGRA_GPU      := drm
TARGET_TEGRA_KEYSTORE := software
TARGET_TEGRA_OMX      := software
TARGET_TEGRA_PBC      := none
TARGET_TEGRA_PHS      := none

$(call inherit-product, device/nvidia/foster/device.mk)

# Fingerprint override
BUILD_FINGERPRINT := NVIDIA/foster_e/foster:9/PPR1.180610.011/4199437_1739.5219:user/release-keys
